/* File containing main() for matrixmangler
 * By: John Jekel (2021)
*/

#include "matrixmanip.h"

int main()
{
    matrix_t testMatrix;
    matrixInit(&testMatrix, 3, 3);

    //Solution: x_1=2 y_1=-1 z_1=1
    testMatrix.rowPtrs[0][0] = 1;
    testMatrix.rowPtrs[1][0] = 2;
    testMatrix.rowPtrs[2][0] = -3;
    testMatrix.rowPtrs[0][1] = -2;
    testMatrix.rowPtrs[1][1] = 1;
    testMatrix.rowPtrs[2][1] = 2;
    testMatrix.rowPtrs[0][2] = 3;
    testMatrix.rowPtrs[1][2] = 1;
    testMatrix.rowPtrs[2][2] = -2;
    testMatrix.solution[0] = 7;
    testMatrix.solution[1] = 4;
    testMatrix.solution[2] = -10;
    matrixPrint(&testMatrix);


    //matrixRowMultiply(&testMatrix, 4, 0);
    //matrixRowAddScalarMultiple(&testMatrix, 2, 3, 0);
    //matrixRowSwap(&testMatrix, 0, 2);
    matrixToRREF(&testMatrix);

    matrixPrint(&testMatrix);

    matrixDeinit(&testMatrix);
    return 0;
}
