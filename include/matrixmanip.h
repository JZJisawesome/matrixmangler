/* File containing definitions for math-related code for matrixmangler
 * By: John Jekel (2021)
 *
 * THINGS ARE ALWAYS ZERO INDEXED
*/

#ifndef MATRIXMANIP_H
#define MATRIXMANIP_H

#include "stdint.h"

//This should never be malloced/freed directly: use matrixInit/Deinit to allocate memory
typedef struct//Augmented matrix
{
    uint32_t rows;
    uint32_t columns;//Not including augmented part

    double* solution;
    double** rowPtrs;//Pointers to rows of the matrix
} matrix_t;

void matrixInit(matrix_t* matrix, uint32_t rows, uint32_t columns);
void matrixDeinit(matrix_t* matrix);
void matrixPrint(matrix_t* matrix);

void matrixRowMultiply(matrix_t* matrix, double factor, uint32_t row);
void matrixRowAddScalarMultiple(matrix_t* matrix, uint32_t row1, double factor, uint32_t row2);
void matrixRowSwap(matrix_t* matrix, uint32_t row1, uint32_t row2);
void matrixToREF(matrix_t* matrix);
void matrixToRREF(matrix_t* matrix);

#endif//MATRIXMANIP_H
