#!/bin/bash

echo "Building"
mkdir -p build
gcc -Wall -g3 -Og -pipe -Iinclude -lpthread ./src/main.c ./src/matrixmanip.c -o build/matrixmanip
echo "Executing"
./build/matrixmanip
